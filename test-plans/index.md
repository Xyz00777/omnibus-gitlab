# Omnibus GitLab test plans

## Updating components

<!-- Keep this list sorted alphabetically. -->
- [golang](upgrade-golang-testplan.md)
- [go-crond](upgrade-go-crond-testplan.md)
- [Mattermost](upgrade-mattermost-testplan.md)
- [redis](upgrade-redis-testplan.md)
